"""

Implementação de uma fila

"""


class Fila():

    def __init__(self):
        self.fila = []

    def implementar(self, valor):
        self.fila.append(valor)
        print (self.fila)

    def __str__(self):
        return '%s' % (self.fila)

Fila().implementar(4)
Fila().implementar(7)
