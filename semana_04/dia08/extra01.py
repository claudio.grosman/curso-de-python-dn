'''

- Classe Bola: Crie uma classe que modele uma bola:
- Atributos: Cor, circunferência, material
- Métodos: trocaCor e mostraCor

'''

class Bola():
    cor = ''
    circunferência = ''
    material = ''

    def __init__(self):
        self.cor = 'indefinida'

    @classmethod
    def trocaCor(cls,corNova):
        cls.cor = corNova
        return 'Cor trocada'

    @classmethod
    def mostraCor(cls):
        return cls.cor

print(Bola().trocaCor('Rosa'))
print(Bola().mostraCor())
