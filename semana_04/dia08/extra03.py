'''
- Classe Retangulo: Crie uma classe que modele um retangulo:
- Atributos: LadoA, LadoB (ou Comprimento e Largura, ou Base e Altura, a escolher)
- Métodos: Mudar valor dos lados, Retornar valor dos lados, calcular Área e calcular Perímetro;

- Crie um programa que utilize esta classe.
- Ele deve pedir ao usuário que informe as medidades de um local.
- Depois, deve criar um objeto com as medidas e calcular a quantidade de pisos e de rodapés necessárias para o local.
'''
from unittest import TestCase

class Retangulo():
    Comprimento = 0
    Largura = 0

    def mudarValoresLados(cls,novoComprimento,novaLargura):
        cls.Comprimento = novoComprimento
        cls.Largura = novaLargura
        return 'Tamanho alterado com sucesso'

    def retornarValorLado(cls):
        return cls.Comprimento,cls.Largura

    def calcularArea(cls):
        return cls.Comprimento*cls.Largura

    def calcularPerimetro(cls):
        return (cls.Comprimento*2)+(cls.Largura*2)



def programa():
    r = Retangulo()
    CompriLocal = int(input('Digite o Comprimento do local: '))
    LargLocal = int(input('Digite a Largura do local: '))
    AlteracaoLocal = r.mudarValoresLados(CompriLocal,LargLocal)
    r.retornarValorLado()
    AreaLocal = r.calcularArea()

    CompriPiso = int(input('Digite o Comprimento do piso: '))
    LargPiso = int(input('Digite a Largura do piso: '))
    AlteracaoPiso = r.mudarValoresLados(CompriPiso,LargPiso)
    AreaPiso = r.calcularArea()

    return f'A quantidade de pisos necessários é de {AreaLocal/AreaPiso}'


print(programa())
