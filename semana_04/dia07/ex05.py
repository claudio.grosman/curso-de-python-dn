'''

- Criar um arquivo de texto usando função open com a tabuada do 10

'''

file = open('tabuada.txt','w')

for x in range(1,11):
    tab = f'{x} x 10 = {x * 10}\n'
    file.writelines(tab)
