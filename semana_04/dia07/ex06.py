'''

- Leia o arquivo criado no ex 05 e transforme cada linha do arquivo em um elemento de uma lista

'''

arqv = open('tabuada.txt').readlines()

lista = []
for x in arqv:
    lista.append(x.strip())
    '''
    OUTRO MÉTODO:

    print([x.strip()])
    '''

print(lista)
