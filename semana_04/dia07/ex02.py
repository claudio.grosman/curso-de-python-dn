"""

- Faça uma função que gere efeito colateral

"""


var = 2

def funcao(x):
    global var
    var += 1
    return x + var

assert funcao(2) == 5
assert funcao(2) == 5
