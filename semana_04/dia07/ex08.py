'''

- Faça uma função chamda 'get_html' que tem como parâmetro uma url.

- A função deve fazer uma chamda a url desejada e salvar seu código html em um arquivo.

'''
from urllib.request import urlopen


def get_html(url):

    request = urlopen(url)

    file = open('example.html', 'bw')

    file.write(request.read())

get_html('https://www.acer.com/ac/pt/BR/content/home')
