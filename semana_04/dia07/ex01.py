"""
Ex 01.

- Faça uma função que sofra efeito colateral por uma variável global.

- Após isso responda se é possível criar um teste para ela.

"""



var = 2

def funcao(x):
    global var
    soma = var + x
    return soma

assert funcao(2) == 4
var = 3
assert funcao(2) == 4
