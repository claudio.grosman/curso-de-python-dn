'''

- Cria uma função que le um arquivo

- Caso nao exista apenas responder que o arquivo nao existe

'''


def ler_arquivo(arquivo):
    try:
        abrir = open(arquivo, 'r')
        print(abrir.read())
    except FileNotFoundError:
        print('Arquivo não existe')

ler_arquivo('tabuada.txt')
