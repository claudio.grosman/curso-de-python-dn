from unittest import TestCase

def cortar(string):
    return string.split()


class TestCortar(TestCase):
    def test_cortar_deve_retornar_lista_Python_e_foda_inserindo_string_Python_e_foda(self):
        inserido ='Python e foda'
        esperado = ['Python','e','foda']
        self.assertEqual(cortar(inserido),esperado)


    def test_cortar_deve_retornar_lista_1_2_be_leza_inserindo_string_1_2_3_be_leza(self):
        inserido = '1 2 3 be leza'
        esperado = ['1','2','3','be','leza']
        self.assertEqual(cortar(inserido),esperado)
