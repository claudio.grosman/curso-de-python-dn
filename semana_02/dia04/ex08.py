'''

- Utilizar Mock's

- Baseado em ATDD e no fato que existe uma função chamada 'sabores_de_pizza'

- Que retorna uma tupla com ('sabor', 'preço': float)

- Faça uma função que recebe um valor: float

- E nos retorna as pizzas com preço <= ao valor passado

'''

from unittest import TestCase, mock

def sabores_de_pizza():
    ...

def retornarPizzas(valor):
    listaDisponivel = []
    for elemento in sabores_de_pizza():
        if elemento[1] <= valor:
            listaDisponivel.append(elemento)
    return listaDisponivel

class Tests(TestCase):
    def test_case_retorna_beringela_11_mussarela_15_com_sabores_mockado_inserindo_15(self):

        with mock.patch('ex08.sabores_de_pizza') as mock_sabores:
            mock_sabores.return_value = [('beringela',11),('mussarela',15),('brocolis',17),('marguerita',22),('calabreza',16)]

            self.assertEqual(retornarPizzas(15),[('beringela',11),('mussarela',15)])
