"""
- Sabendo que existe uma função 'get-users' que faz um chamada no banco de dados.

- Quando ela é chamada retorna uma lista de tuplas com os seguintes valores:
     - (id,nome,email) -> (507,Jorge,jorgin@hotmail.com)

- Faça uma função 'filter_users' onde serão retornados apenas os usuários com id superior a 100.

"""

from unittest import TestCase, mock

def get_users():
    ...

def filter_users(lista):
    maioresDe100 = []
    for elemento in lista:
        if elemento[0] >= 100:
            maioresDe100.append(elemento)
    return maioresDe100

class Test(TestCase):
   def test_get_users_retorna_lista_de_tuplas_(self):

       esperado = [(109,'Marcos','marcão@hotmail.com'),(509,'tereza','terezinha@gmail.com')]

       with mock.patch('ex06.get_users') as mock_users:
           mock_users.return_value = [(1,'Bruna','bruninha@hotmail.com'),(47,'Manarui','mana@yahoo.com.br'),(109,'Marcos','marcão@hotmail.com'),(509,'tereza','terezinha@gmail.com')]
           self.assertEqual(filter_users(get_users()),esperado)
