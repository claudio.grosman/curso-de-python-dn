from unittest import TestCase

from collections import Counter


def contar(texto):
    return Counter(texto)


print(contar('Halo'))

class TestContar(TestCase):
    def test_frequencia_retornar_dicionario_quando_inserido_string(self):
        inserido = 'Macaco'
        esperado = {'M': 1, 'a':2, 'c':2, 'o':1}
        self.assertEquals(contar(inserido),esperado)

    def test_frequencia_retorna_contagem_inserindo__(self):
        inserido = ''
        esperado = {}
        self.assertEquals(contar(inserido),esperado)

    def test_frequencia_retorna_1_inserindo__espaço(self):
        inserido = ' '
        esperado = {' ': 1}
        self.assertEquals(contar(inserido),esperado)
