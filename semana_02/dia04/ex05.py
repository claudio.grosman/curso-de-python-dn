from unittest import TestCase
from collections import Counter
from ex03 import contar
from ex04 import cortar



def conta_e_cortar(texto):
    return Counter(cortar(texto))


class Test_Contagem_Lista(TestCase):
    def test_conta_e_cortar_retorna_dicionario_inserindo_string(self):
        inserido = 'Python e daora'
        esperado = {'Python': 1,'e':1,'daora':1}
        self.assertEquals(conta_e_cortar(inserido),esperado)


    def test_conta_e_cortar_retorna_Python_2_legal_1_inserindo_Python_Python_legal(self):
        inserido = 'Python Python legal'
        esperado = {'Python': 2,'legal':1}
        self.assertEquals(conta_e_cortar(inserido),esperado)
