from unittest import TestCase, mock

def soma(x,y):
    return x + y

def sub(x,y):
    return x - y

def exp(x,y,z):
    return sub(soma(x,y),z)

class Text_Exp(TestCase):
    def test_input_indireto_exp(self):
        x = 1
        y = 2
        z = 3
        with mock.patch('ex11.soma') as mock_soma:
            exp(x,y,z)

        mock_soma.assert_called_with(x,y)
