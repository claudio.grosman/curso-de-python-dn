from unittest import TestCase

class Calc:
    def add (self, x,y):
        return x + y

    def sub(self,x,y):
        return x + y

    def mult(self, x, y):
        return x + y

    def div(self, x,y):
        return x + y


class TestCalc(TestCase):
    def test_add_precisa_retornar_2_com_1_1(self):
        self.assertEquals(Calc().add(1,1),2)

    def test_sub_precisa_retornar_0_com_1_1(self):
        self.assertEquals(Calc().sub(1, 1), 0)

    def test_mult_precisa_retornar_2_com_2_1(self):
        self.assertEquals(Calc().mult(2, 1), 2)

    def test_div_precisa_retornar_2_com_4_2(self):
        self.assertEquals(Calc().div(4, 2), 2)


