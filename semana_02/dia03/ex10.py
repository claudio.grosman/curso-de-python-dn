from unittest import TestCase


def soma(x, y):
    return x + y


def sub(x, y):
    return x - y


def exp(x, y, z):
    return sub(soma(x, y), z)


class Test_Exp(TestCase):

    dummy = 0

    def test_indireto_sub(self):
        self.assertEquals(exp(exp(1, 1, self.dummy), 1, self.dummy), 3)