from unittest import TestCase

def soma (x,y):
    return x + y

def sub(x,y):
    return x - y

def exp(x,y,z):
    return sub(soma(x,y),z)

class Test_Exp(TestCase):
    def teste_exp_precisa_retornar_1_com_1_1_1(self):
        self.assertEquals(exp(exp(1,1,1),1,1),1)