from unittest import TestCase

def soma (x,y):
    return x + y

def sub(x,y):
    return x - y

def exp(x,y,z):
    return sub(soma(x,y),z)


class Test_Exp(TestCase):

    dummy = 0

    def test_indireto_soma(self):
        self.assertEquals(exp(self.dummy,self.dummy, 2), -2)