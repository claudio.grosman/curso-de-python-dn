# language:pt
'''
- Dado um jogo Jokenpo
- Como seria possível descrever sua funcionalidade geral?
'''

Funcionalidade: Compra de produtos

Cenário: Compra de um produto com frete grátis em região especifica
  Dado que eu sou um cliente
  Quando eu comprar um produto de qualquer categoria
    E o valor total da compra for igual ou maior a R$ 100
    E meu CEP pertencer a região Sul
  Então o frete será grátis
