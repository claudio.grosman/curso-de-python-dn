lista = [1,2,3,4,5]
quart = 0

def quartil(lista):
    op = int(input('1 - Quartil inferior\n2 - Quartil do meio\n3 - Quartil superior:'))

    if op == 1:
        qntd = len(lista)
        valor = int(0.25 * qntd)
        return sorted(lista)[valor]

    elif op == 2:
        qntd = len(lista)
        valor = int(0.5 * qntd)
        return sorted(lista)[valor]

    elif op == 3:
        qntd = len(lista)
        valor = int(0.75 * qntd)
        return sorted(lista)[valor]
    else:
        return 'Voce inseriu opção errada'

print(quartil(lista))
