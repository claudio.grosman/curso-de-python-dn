from unittest import TestCase

lista = []
lista2 = []

def acum(lista):
    acum = 0
    for x in lista:
        acum += x
        lista2.append(acum)
    return lista2


class TestAcum(TestCase):
    def test_acum_inserindo_lista_1_2_3_retorna_lista_1_3_6(self):
        inserido = [1,2,3,4]
        esperado = [1,3,6,10]
        self.assertEqual(acum(inserido),esperado)
