lista = [1,2,3,4,5]

d = {}
d['Lista'] = lista
d['Somatório da lista'] = sum(lista)
d['Tamanho da lista'] = len(lista)
d['Maior valor'] = max(lista)
d['Menor valor'] = min(lista)

print(d)