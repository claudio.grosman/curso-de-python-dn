from unittest import TestCase

lista = [1,2,3,4,5]

def mediana(lista):
    qntd_itens = len(lista)
    ordenar = sorted(lista)
    meio = int(qntd_itens/2)
    mediana = ordenar[meio]
    return mediana

class Test_Mediana(TestCase):
    def test_mediana_retorna_2_com_lista_de_1_a_3(self):
        self.assertEquals(mediana([1,2,3]),2)


