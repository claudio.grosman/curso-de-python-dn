from unittest import TestCase

lista = [1,2,3]

def media(lista):
    tamanho = len(lista)
    somatorio = sum(lista)
    media = somatorio/tamanho
    return media

class TestMedia(TestCase):
    def test_media_retorna_2_com_lista_2_2(self):
        self.assertEquals(media([2,2]),2)