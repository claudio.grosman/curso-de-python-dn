#dispersão (maior menos o menor valor da lista)
from unittest import TestCase

lista = [1,2,3,4,5]

def dispersao(lista):
    return max(lista) - min(lista)

class Test_Dispersao(TestCase):
    def test_dispersao_retorna_2_com_lista_de_1_a_3(self):
        self.assertEquals(dispersao([1,2,3]),2)
