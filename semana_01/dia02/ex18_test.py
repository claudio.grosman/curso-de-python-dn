from unittest import TestCase

def funcao(num):

    rj = "Romeu e Julieta"
    q = 'Queijo'
    g = 'Goiabada'

    if num % 3 == 0 and num % 5 == 0:
        return rj

    elif num % 3 == 0:
        return q

    elif num % 5 == 0:
        return g

    else:
        return num


class Test_Funcao(TestCase):
    def test_funcao_entrada_3_retornar_queijo(self):
        self.assertEquals(funcao(3),'Queijo')

    def test_funcao_entrada_5_retorna_goiabada(self):
        self.assertEquals(funcao(5),'Goiabada')

    def test_funcao_entrada_30_retorna_romeu_e_julieta(self):
        self.assertEquals(funcao(30),'Romeu e Julieta')

    def test_funcao_entrada_22_retorna_22(self):
        self.assertEquals(funcao(22),22)