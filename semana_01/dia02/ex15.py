from unittest import TestCase

def soma_2(x):
    return x + 2

def sub_2(x):
    return x - 2

def reaplicar(func,x):
    return func(func(x))


print(reaplicar(soma_2,2))
print(reaplicar(sub_2,2))