from unittest import TestCase

lista = ['keep','remove','keep','remove','keep','remove']

def filtrar(lista):
    saida = list(filter(lambda x: x != 'remove', lista))
    return saida

class Test_filtrar(TestCase):
    def test_case_retorna_keep_keep_com_entrada_de_lista_keep_remove_keep_remove(self):
        self.assertEquals(filtrar(['keep','remove','keep','remove']),['keep','keep'])
