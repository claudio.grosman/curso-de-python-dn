from unittest import TestCase


def funcao(num):
    rj = 'Romeu e Julieta'
    if num % 3 == 0 and num % 5 == 0:
        return rj
    else:
        return num


class Test_Funcao(TestCase):
    def test_funcao_retorna_romeu_e_julieta_inserindo_numero_15(self):
        self.assertEquals(funcao(15),'Romeu e Julieta')
