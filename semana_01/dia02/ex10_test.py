from unittest import TestCase


def funcao(num,string):
    qntd = len(string)
    if qntd == num:
        return True
    else:
        return False

class Test_Funcao(TestCase):
    def test_funcao_retorna_true_inserindo_2_Ab(self):
        self.assertEquals(funcao(2,'Ab'), True)

    def test_funcao_retorna_false_inserindo_3_abcd(self):
        self.assertEquals(funcao(3,'abcd'), False)