from unittest import TestCase

lista1 = [1,2,3,4,5]

def inversao(lista1):
    for entrada in lista1:
        lista1.remove(entrada)
        lista1.insert(0,entrada)
    return lista1


class Test_Inversao(TestCase):
    def test_inversao_retorna_lista_de_3_a_1_com_lista_de_1_a_3(self):
        self.assertEquals(inversao([1,2,3]),[3,2,1])