from functools import reduce
from unittest import TestCase

lista = ['keep','remove']

def contar(lista):
    lista = len(list(reduce(lambda x,y: x+y,lista)))
    return lista

class Test_Contar(TestCase):
    def test_contar_retorna_2_com_entrada_de_lista_a_b(self):
        self.assertEquals(contar(['a','b']),2)