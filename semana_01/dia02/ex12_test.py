from unittest import TestCase

def funcao(num):

    q = 'queijo'

    if num % 3 == 0:
        return q

    else:
        return num


class Test_Funcao(TestCase):
    def test_funcao_retorna_queijo_inserindo_3(self):
        self.assertEquals(funcao(3),'queijo')

    def test_funcao_retorna_2_inserindo_2(self):
        self.assertEquals(funcao(2),2)