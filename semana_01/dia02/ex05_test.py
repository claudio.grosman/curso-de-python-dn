from unittest import TestCase

list = ['foo','bar','spam','eggs']

list2 = []

def duplicar(list):
    for entrada in list:
        list2.append(entrada+entrada)

    return list2

class Test_Duplicar(TestCase):
    def test_duplicar_retorna_foofoo_com_entrada_de_uma_lista_foo(self):
        self.assertEquals(duplicar(['foo']),['foofoo'])
