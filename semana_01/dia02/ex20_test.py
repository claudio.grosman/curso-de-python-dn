from unittest import TestCase

lista = ['keep','remove','keep','remove','keep','remove']

def maiuscula(lista):
    lista = list(map(lambda primeira_letra: primeira_letra.capitalize(),lista))
    return lista

class Test_Maiuscula(TestCase):
    def test_maiuscula_retorna_Keep_Remove_com_entrada_de_lista_keep_remove(self):
        self.assertEquals(maiuscula(['keep','remove']),['Keep','Remove'])