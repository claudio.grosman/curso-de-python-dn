from unittest import TestCase

def soma_1(x):
    return x+1

def aplicar(func,valor):
    lista2 = []
    for elemento in range(valor):
        lista2.append(func(elemento))
    return lista2

class Test_Aplicar(TestCase):
    def test_aplicar_retorna_1_e_2_quando_inseridos_1_e_2(self):
        self.assertEquals(aplicar(soma_1(1)),2,[1,2])