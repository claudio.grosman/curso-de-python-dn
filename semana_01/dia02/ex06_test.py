from unittest import TestCase

lista = ['foo','bar','spam','eggs']

def duplicar(lista):
    lista = list(map(lambda x: x+x,lista))
    return lista

class Test_Duplicar(TestCase):
    def test_duplicar_retorna_foofoo_com_entrada_de_lista_foo(self):
        self.assertEquals(duplicar(['foo']),['foofoo'])