from unittest import TestCase

def funcao(num):
    g = 'Goiabada'
    if num % 5 == 0: #se o resto da divisal por 3 for 0 ele é divisivel por 5
        return g

    else:
        return num

class Test_Funcao(TestCase):
    def test_funcao_retorna_Goiabada_inserindo_valor_5(self):
        self.assertEquals(funcao(5),'Goiabada')

    def test_funcao_retorna_3_inserindo_valor_3(self):
        self.assertEquals(funcao(3),3)