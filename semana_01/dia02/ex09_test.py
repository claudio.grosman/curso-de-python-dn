from unittest import TestCase

def funcao(saudacao,nome):
    resultado = saudacao + (' ') + nome
    return resultado

class Test_Funcao(TestCase):
    def test_funcao_retornar_Oi_Henrique_quando_inserido_Oi_Henrique(self):
        self.assertEquals(funcao('Oi','Henrique'),'Oi Henrique')