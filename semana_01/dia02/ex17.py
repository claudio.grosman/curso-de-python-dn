lista = [1,2,3,4]

def soma_2(x):
    return x+2

def sub_2(x):
    return x-2

def aplicar(func,lista):
    lista2 = []
    for elemento in lista:
        lista2.append(func(elemento))
    return lista2

print(aplicar(soma_2,lista))
print(aplicar(sub_2,lista))

